
# OH WEEKLY ASSESSMENT 2

  

  

In this assessment, you have to answer some basic question about Internet. You can find the answer from the internet or by discussing with your friend, but make sure you understand what you are doing. Please deliver this assessment on time no matter what results you make! Thank you.

  

Deadline: Saturday, 28 December 2021, 23:59 WIB


## Instructions
- Clone this project repository
- Try to complete complete the task
- Create git repository using `weekly-assessment-2` as the name of project
- Push your result to it
- Fill this form when you are finish: https://docs.google.com/forms/d/e/1FAIpQLSfe-kDIu-efXh_62tq3dAl3DwuN9PB2Pae0ywYPKSMUiM0sOw/viewform


## Task Requirements
- [ ] Complete: Answer all Question
  
  

Question:

- Explain How the Internet Works.

- What's the meaning of communication protocol

- Mention HTTP status codes

- Make Analogy Regarding how Front end and Backend communicate each other

- Mention What method in HTTP protocol

  
  ## Please answer this question in markdown format